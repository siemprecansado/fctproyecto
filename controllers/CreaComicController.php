<?php

namespace app\controllers;

use Yii;
use app\models\CreaComic;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CreaComicController implements the CRUD actions for CreaComic model.
 */
class CreaComicController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CreaComic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CreaComic::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CreaComic model.
     * @param string $idcreador
     * @param string $idcomic
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idcreador, $idcomic)
    {
        return $this->render('view', [
            'model' => $this->findModel($idcreador, $idcomic),
        ]);
    }

    /**
     * Creates a new CreaComic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CreaComic();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idcreador' => $model->idcreador, 'idcomic' => $model->idcomic]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CreaComic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idcreador
     * @param string $idcomic
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idcreador, $idcomic)
    {
        $model = $this->findModel($idcreador, $idcomic);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idcreador' => $model->idcreador, 'idcomic' => $model->idcomic]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CreaComic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idcreador
     * @param string $idcomic
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idcreador, $idcomic)
    {
        $this->findModel($idcreador, $idcomic)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CreaComic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idcreador
     * @param string $idcomic
     * @return CreaComic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idcreador, $idcomic)
    {
        if (($model = CreaComic::findOne(['idcreador' => $idcreador, 'idcomic' => $idcomic])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
