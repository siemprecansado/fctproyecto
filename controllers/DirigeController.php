<?php

namespace app\controllers;

use Yii;
use app\models\Dirige;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirigeController implements the CRUD actions for Dirige model.
 */
class DirigeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dirige models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Dirige::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dirige model.
     * @param string $iddirector
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($iddirector, $idpelicula)
    {
        return $this->render('view', [
            'model' => $this->findModel($iddirector, $idpelicula),
        ]);
    }

    /**
     * Creates a new Dirige model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dirige();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'iddirector' => $model->iddirector, 'idpelicula' => $model->idpelicula]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Dirige model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $iddirector
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($iddirector, $idpelicula)
    {
        $model = $this->findModel($iddirector, $idpelicula);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'iddirector' => $model->iddirector, 'idpelicula' => $model->idpelicula]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dirige model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $iddirector
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($iddirector, $idpelicula)
    {
        $this->findModel($iddirector, $idpelicula)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Dirige model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $iddirector
     * @param string $idpelicula
     * @return Dirige the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($iddirector, $idpelicula)
    {
        if (($model = Dirige::findOne(['iddirector' => $iddirector, 'idpelicula' => $idpelicula])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
