<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dirige".
 *
 * @property string $iddirector
 * @property string $idpelicula
 *
 * @property Directorpeliculas $iddirector0
 * @property Peliculas $idpelicula0
 */
class Dirige extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dirige';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddirector', 'idpelicula'], 'required'],
            [['iddirector', 'idpelicula'], 'string', 'max' => 255],
            [['iddirector', 'idpelicula'], 'unique', 'targetAttribute' => ['iddirector', 'idpelicula']],
            [['iddirector'], 'exist', 'skipOnError' => true, 'targetClass' => Directorpeliculas::className(), 'targetAttribute' => ['iddirector' => 'iddirector']],
            [['idpelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['idpelicula' => 'idpelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddirector' => 'Iddirector',
            'idpelicula' => 'Idpelicula',
        ];
    }

    /**
     * Gets query for [[Iddirector0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirector0()
    {
        return $this->hasOne(Directorpeliculas::className(), ['iddirector' => 'iddirector']);
    }

    /**
     * Gets query for [[Idpelicula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpelicula0()
    {
        return $this->hasOne(Peliculas::className(), ['idpelicula' => 'idpelicula']);
    }
}
