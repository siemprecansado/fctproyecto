<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property string $idlibro
 * @property string|null $editorial
 * @property int|null $num_ediciones
 *
 * @property Autor $autor
 * @property Escribe[] $escribes
 * @property Autor[] $idautors
 * @property Media $idlibro0
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idlibro'], 'required'],
            [['num_ediciones'], 'integer'],
            [['idlibro', 'editorial'], 'string', 'max' => 20],
            [['idlibro'], 'unique'],
            [['idlibro'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idlibro' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlibro' => 'Idlibro',
            'editorial' => 'Editorial',
            'num_ediciones' => 'Num Ediciones',
        ];
    }

    /**
     * Gets query for [[Autor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autor::className(), ['idautor' => 'idlibro']);
    }

    /**
     * Gets query for [[Escribes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribes()
    {
        return $this->hasMany(Escribe::className(), ['idlibro' => 'idlibro']);
    }

    /**
     * Gets query for [[Idautors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdautors()
    {
        return $this->hasMany(Autor::className(), ['idautor' => 'idautor'])->viaTable('escribe', ['idlibro' => 'idlibro']);
    }

    /**
     * Gets query for [[Idlibro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdlibro0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idlibro']);
    }
}
