<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directorpeliculas".
 *
 * @property string $iddirector
 * @property string|null $director
 *
 * @property Peliculas $iddirector0
 * @property Dirige[] $diriges
 * @property Peliculas[] $idpeliculas
 */
class Directorpeliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directorpeliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddirector'], 'required'],
            [['iddirector', 'director'], 'string', 'max' => 20],
            [['iddirector'], 'unique'],
            [['iddirector'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['iddirector' => 'idpelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddirector' => 'Iddirector',
            'director' => 'Director',
        ];
    }

    /**
     * Gets query for [[Iddirector0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirector0()
    {
        return $this->hasOne(Peliculas::className(), ['idpelicula' => 'iddirector']);
    }

    /**
     * Gets query for [[Diriges]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiriges()
    {
        return $this->hasMany(Dirige::className(), ['iddirector' => 'iddirector']);
    }

    /**
     * Gets query for [[Idpeliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpeliculas()
    {
        return $this->hasMany(Peliculas::className(), ['idpelicula' => 'idpelicula'])->viaTable('dirige', ['iddirector' => 'iddirector']);
    }
}
