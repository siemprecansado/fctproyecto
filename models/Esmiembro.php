<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "esmiembro".
 *
 * @property string $idmiembro
 * @property string $idpodcast
 *
 * @property Miembro $idmiembro0
 * @property Podcasts $idpodcast0
 */
class Esmiembro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'esmiembro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmiembro', 'idpodcast'], 'required'],
            [['idmiembro', 'idpodcast'], 'string', 'max' => 255],
            [['idmiembro', 'idpodcast'], 'unique', 'targetAttribute' => ['idmiembro', 'idpodcast']],
            [['idmiembro'], 'exist', 'skipOnError' => true, 'targetClass' => Miembro::className(), 'targetAttribute' => ['idmiembro' => 'idmiembro']],
            [['idpodcast'], 'exist', 'skipOnError' => true, 'targetClass' => Podcasts::className(), 'targetAttribute' => ['idpodcast' => 'idpodcast']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmiembro' => 'Idmiembro',
            'idpodcast' => 'Idpodcast',
        ];
    }

    /**
     * Gets query for [[Idmiembro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmiembro0()
    {
        return $this->hasOne(Miembro::className(), ['idmiembro' => 'idmiembro']);
    }

    /**
     * Gets query for [[Idpodcast0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpodcast0()
    {
        return $this->hasOne(Podcasts::className(), ['idpodcast' => 'idpodcast']);
    }
}
