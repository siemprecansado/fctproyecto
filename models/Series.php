<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "series".
 *
 * @property string $idserie
 * @property string|null $canal
 * @property int|null $temporadas
 * @property int|null $episodios
 *
 * @property Directorseries $directorseries
 * @property Dirigeseries[] $dirigeseries
 * @property Directorseries[] $iddirectors
 * @property Media $idserie0
 */
class Series extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idserie'], 'required'],
            [['temporadas', 'episodios'], 'integer'],
            [['idserie', 'canal'], 'string', 'max' => 20],
            [['idserie'], 'unique'],
            [['idserie'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idserie' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idserie' => 'Idserie',
            'canal' => 'Canal',
            'temporadas' => 'Temporadas',
            'episodios' => 'Episodios',
        ];
    }

    /**
     * Gets query for [[Directorseries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorseries()
    {
        return $this->hasOne(Directorseries::className(), ['iddirectorserie' => 'idserie']);
    }

    /**
     * Gets query for [[Dirigeseries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirigeseries()
    {
        return $this->hasMany(Dirigeseries::className(), ['idserie' => 'idserie']);
    }

    /**
     * Gets query for [[Iddirectors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirectors()
    {
        return $this->hasMany(Directorseries::className(), ['iddirectorserie' => 'iddirector'])->viaTable('dirigeseries', ['idserie' => 'idserie']);
    }

    /**
     * Gets query for [[Idserie0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdserie0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idserie']);
    }
}
