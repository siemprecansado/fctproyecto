<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property string $idpelicula
 * @property string|null $productora
 *
 * @property Directorpeliculas $directorpeliculas
 * @property Dirige[] $diriges
 * @property Directorpeliculas[] $iddirectors
 * @property Es[] $es
 * @property Genero[] $idgeneros
 * @property Genero $genero
 * @property Media $idpelicula0
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpelicula'], 'required'],
            [['idpelicula', 'productora'], 'string', 'max' => 20],
            [['idpelicula'], 'unique'],
            [['idpelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idpelicula' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpelicula' => 'Idpelicula',
            'productora' => 'Productora',
        ];
    }

    /**
     * Gets query for [[Directorpeliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorpeliculas()
    {
        return $this->hasOne(Directorpeliculas::className(), ['iddirector' => 'idpelicula']);
    }

    /**
     * Gets query for [[Diriges]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDiriges()
    {
        return $this->hasMany(Dirige::className(), ['idpelicula' => 'idpelicula']);
    }

    /**
     * Gets query for [[Iddirectors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirectors()
    {
        return $this->hasMany(Directorpeliculas::className(), ['iddirector' => 'iddirector'])->viaTable('dirige', ['idpelicula' => 'idpelicula']);
    }

    /**
     * Gets query for [[Es]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEs()
    {
        return $this->hasMany(Es::className(), ['idpelicula' => 'idpelicula']);
    }

    /**
     * Gets query for [[Idgeneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgeneros()
    {
        return $this->hasMany(Genero::className(), ['idgenero' => 'idgenero'])->viaTable('es', ['idpelicula' => 'idpelicula']);
    }

    /**
     * Gets query for [[Genero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenero()
    {
        return $this->hasOne(Genero::className(), ['idgenero' => 'idpelicula']);
    }

    /**
     * Gets query for [[Idpelicula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpelicula0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idpelicula']);
    }
}
