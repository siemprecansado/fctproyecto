<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hacen".
 *
 * @property string $idusuario
 * @property string $idreview
 *
 * @property Usuarios $idusuario0
 * @property Reviews $idreview0
 */
class Hacen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hacen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idusuario', 'idreview'], 'required'],
            [['idusuario', 'idreview'], 'string', 'max' => 20],
            [['idusuario', 'idreview'], 'unique', 'targetAttribute' => ['idusuario', 'idreview']],
            [['idusuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['idusuario' => 'idusuario']],
            [['idreview'], 'exist', 'skipOnError' => true, 'targetClass' => Reviews::className(), 'targetAttribute' => ['idreview' => 'idreview']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusuario' => 'Idusuario',
            'idreview' => 'Idreview',
        ];
    }

    /**
     * Gets query for [[Idusuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuario0()
    {
        return $this->hasOne(Usuarios::className(), ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Idreview0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdreview0()
    {
        return $this->hasOne(Reviews::className(), ['idreview' => 'idreview']);
    }
}
