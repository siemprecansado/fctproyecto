<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "creadororiginal".
 *
 * @property string $idcreador
 * @property string|null $creador_original
 *
 * @property Creacomic[] $creacomics
 * @property Comics[] $idcomics
 * @property Comics $idcreador0
 */
class Creadororiginal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'creadororiginal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcreador'], 'required'],
            [['idcreador', 'creador_original'], 'string', 'max' => 20],
            [['idcreador'], 'unique'],
            [['idcreador'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['idcreador' => 'idcomic']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcreador' => 'Idcreador',
            'creador_original' => 'Creador Original',
        ];
    }

    /**
     * Gets query for [[Creacomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreacomics()
    {
        return $this->hasMany(Creacomic::className(), ['idcreador' => 'idcreador']);
    }

    /**
     * Gets query for [[Idcomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcomics()
    {
        return $this->hasMany(Comics::className(), ['idcomic' => 'idcomic'])->viaTable('creacomic', ['idcreador' => 'idcreador']);
    }

    /**
     * Gets query for [[Idcreador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcreador0()
    {
        return $this->hasOne(Comics::className(), ['idcomic' => 'idcreador']);
    }
}
