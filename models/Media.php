<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "media".
 *
 * @property string $idmedia
 * @property float|null $media_usuario
 * @property float|null $puntuacion
 * @property string|null $nombre
 * @property string|null $fecha_lanzamiento
 * @property float|null $puntuacion_criticos
 *
 * @property Albumes $albumes
 * @property Comics $comics
 * @property Guardan[] $guardans
 * @property Usuarios[] $idusuarios
 * @property Libros $libros
 * @property Peliculas $peliculas
 * @property Podcasts $podcasts
 * @property Series $series
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmedia'], 'required'],
            [['media_usuario', 'puntuacion', 'puntuacion_criticos'], 'number'],
            [['fecha_lanzamiento'], 'safe'],
            [['idmedia', 'nombre'], 'string', 'max' => 20],
            [['idmedia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmedia' => 'Idmedia',
            'media_usuario' => 'Media Usuario',
            'puntuacion' => 'Puntuacion',
            'nombre' => 'Nombre',
            'fecha_lanzamiento' => 'Fecha Lanzamiento',
            'puntuacion_criticos' => 'Puntuacion Criticos',
        ];
    }

    /**
     * Gets query for [[Albumes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlbumes()
    {
        return $this->hasOne(Albumes::className(), ['idalbum' => 'idmedia']);
    }

    /**
     * Gets query for [[Comics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComics()
    {
        return $this->hasOne(Comics::className(), ['idcomic' => 'idmedia']);
    }

    /**
     * Gets query for [[Guardans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuardans()
    {
        return $this->hasMany(Guardan::className(), ['idmedia' => 'idmedia']);
    }

    /**
     * Gets query for [[Idusuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuarios()
    {
        return $this->hasMany(Usuarios::className(), ['idusuario' => 'idusuario'])->viaTable('guardan', ['idmedia' => 'idmedia']);
    }

    /**
     * Gets query for [[Libros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasOne(Libros::className(), ['idlibro' => 'idmedia']);
    }

    /**
     * Gets query for [[Peliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeliculas()
    {
        return $this->hasOne(Peliculas::className(), ['idpelicula' => 'idmedia']);
    }

    /**
     * Gets query for [[Podcasts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPodcasts()
    {
        return $this->hasOne(Podcasts::className(), ['idpodcast' => 'idmedia']);
    }

    /**
     * Gets query for [[Series]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSeries()
    {
        return $this->hasOne(Series::className(), ['idserie' => 'idmedia']);
    }
}
