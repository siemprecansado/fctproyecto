<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "miembro".
 *
 * @property string $idmiembro
 * @property string|null $miembro
 *
 * @property Esmiembro[] $esmiembros
 * @property Podcasts[] $idpodcasts
 * @property Podcasts $idmiembro0
 */
class Miembro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'miembro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmiembro'], 'required'],
            [['idmiembro', 'miembro'], 'string', 'max' => 20],
            [['idmiembro'], 'unique'],
            [['idmiembro'], 'exist', 'skipOnError' => true, 'targetClass' => Podcasts::className(), 'targetAttribute' => ['idmiembro' => 'idpodcast']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmiembro' => 'Idmiembro',
            'miembro' => 'Miembro',
        ];
    }

    /**
     * Gets query for [[Esmiembros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEsmiembros()
    {
        return $this->hasMany(Esmiembro::className(), ['idmiembro' => 'idmiembro']);
    }

    /**
     * Gets query for [[Idpodcasts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpodcasts()
    {
        return $this->hasMany(Podcasts::className(), ['idpodcast' => 'idpodcast'])->viaTable('esmiembro', ['idmiembro' => 'idmiembro']);
    }

    /**
     * Gets query for [[Idmiembro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmiembro0()
    {
        return $this->hasOne(Podcasts::className(), ['idpodcast' => 'idmiembro']);
    }
}
