<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Podcasts */

$this->title = 'Create Podcasts';
$this->params['breadcrumbs'][] = ['label' => 'Podcasts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="podcasts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
