<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirectorPeliculas */

$this->title = 'Update Director Peliculas: ' . $model->iddirector;
$this->params['breadcrumbs'][] = ['label' => 'Director Peliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddirector, 'url' => ['view', 'id' => $model->iddirector]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="director-peliculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
