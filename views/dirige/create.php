<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dirige */

$this->title = 'Create Dirige';
$this->params['breadcrumbs'][] = ['label' => 'Diriges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirige-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
