<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dirige */

$this->title = 'Update Dirige: ' . $model->iddirector;
$this->params['breadcrumbs'][] = ['label' => 'Diriges', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddirector, 'url' => ['view', 'iddirector' => $model->iddirector, 'idpelicula' => $model->idpelicula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dirige-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
