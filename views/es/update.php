<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Es */

$this->title = 'Update Es: ' . $model->idgenero;
$this->params['breadcrumbs'][] = ['label' => 'Es', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idgenero, 'url' => ['view', 'idgenero' => $model->idgenero, 'idpelicula' => $model->idpelicula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="es-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
