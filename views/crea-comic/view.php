<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CreaComic */

$this->title = $model->idcreador;
$this->params['breadcrumbs'][] = ['label' => 'Crea Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="crea-comic-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idcreador' => $model->idcreador, 'idcomic' => $model->idcomic], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idcreador' => $model->idcreador, 'idcomic' => $model->idcomic], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idcreador',
            'idcomic',
        ],
    ]) ?>

</div>
