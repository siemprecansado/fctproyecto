<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Albumes', 'url' => ['/site/index']],
            ['label' => 'Autor', 'url' => ['/site/index']],
            ['label' => 'Comics', 'url' => ['/site/index']],
            ['label' => 'Creacomic', 'url' => ['/site/index']],
            ['label' => 'Creadororiginal', 'url' => ['/site/index']],
            ['label' => 'directorpeliculas', 'url' => ['/site/index']],
            ['label' => 'Directorseries', 'url' => ['/site/index']],
            ['label' => 'Dirige', 'url' => ['/site/index']],
            ['label' => 'Dirigeseries', 'url' => ['/site/index']],
            ['label' => 'Es', 'url' => ['/site/index']],
            ['label' => 'Escribe', 'url' => ['/site/index']],
            ['label' => 'Esmiembro', 'url' => ['/site/index']],
            ['label' => 'Genero', 'url' => ['/site/index']],
            ['label' => 'Guardan', '
            url' => ['/site/index']],
            ['label' => 'Hacen', 'url' => ['/site/index']],
            ['label' => 'Libros', 'url' => ['/site/index']],
            ['label' => 'Media', 'url' => ['/site/index']],
            ['label' => 'Miembro', 'url' => ['/site/index']],
            ['label' => 'Peliculas', 'url' => ['/site/index']],
            ['label' => 'Podcasts', 'url' => ['/site/index']],
            ['label' => 'Reviews', 'url' => ['/site/index']],
            ['label' => 'Series', 'url' => ['/site/index']],
            ['label' => 'Usuarios', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
