<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirigeSeries */

$this->title = 'Create Dirige Series';
$this->params['breadcrumbs'][] = ['label' => 'Dirige Series', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirige-series-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
