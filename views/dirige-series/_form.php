<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DirigeSeries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dirige-series-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idserie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'iddirector')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
