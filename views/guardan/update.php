<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Guardan */

$this->title = 'Update Guardan: ' . $model->idusuario;
$this->params['breadcrumbs'][] = ['label' => 'Guardans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idusuario, 'url' => ['view', 'idusuario' => $model->idusuario, 'idmedia' => $model->idmedia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="guardan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
